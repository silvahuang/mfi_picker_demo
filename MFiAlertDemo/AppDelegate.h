//
//  AppDelegate.h
//  MFiAlertDemo
//
//  Created by Anker on 2018/8/2.
//  Copyright © 2018 Anker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

