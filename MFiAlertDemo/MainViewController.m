//
//  MainViewController.m
//  MFiAlertDemo
//
//  Created by Anker on 2018/8/2.
//  Copyright © 2018 Anker. All rights reserved.
//

#import "MainViewController.h"
#import "MFiAlertViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onJumpButtonTouched:(id)sender {
    UIStoryboard* sb = self.storyboard;
    MFiAlertViewController* mfiVC = [sb instantiateViewControllerWithIdentifier:@"MFiAlert"];
    [self.navigationController pushViewController:mfiVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
