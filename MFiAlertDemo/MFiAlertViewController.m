//
//  ViewController.m
//  MFiAlertDemo
//
//  Created by Anker on 2018/8/2.
//  Copyright © 2018 Anker. All rights reserved.
//

#import "MFiAlertViewController.h"
#import <ExternalAccessory/ExternalAccessory.h>

@interface MFiAlertViewController () {
    
    __weak IBOutlet UILabel *_stateLb;
}

@end

@implementation MFiAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)onShowMFiAlertButtonTouched:(id)sender {
//    [self readValueFromPastedBoard];
    
    NSString* name = @"ABCD";
    NSString *string = [NSString stringWithFormat:@"self CONTAINS[c] '%@'", name];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:string];
    [[EAAccessoryManager sharedAccessoryManager] showBluetoothAccessoryPickerWithNameFilter:predicate completion:^(NSError * _Nullable error) {
        NSLog(@"connect accessory: %@, %@", [[EAAccessoryManager sharedAccessoryManager] connectedAccessories], error);
    }];
}

- (IBAction)onTestLeftButtonTouched:(id)sender {
    _stateLb.text = [NSString stringWithFormat:@"left button touched(%d)",arc4random_uniform(10000)];
}

- (IBAction)onTestRightButtonTouched:(id)sender {
    _stateLb.text = [NSString stringWithFormat:@"right button touched(%d)",arc4random_uniform(10000)];
}

@end
